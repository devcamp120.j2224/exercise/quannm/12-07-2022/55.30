package com.devcamp.hellopizza365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hellopizza365Application {

	public static void main(String[] args) {
		SpringApplication.run(Hellopizza365Application.class, args);
	}

}
