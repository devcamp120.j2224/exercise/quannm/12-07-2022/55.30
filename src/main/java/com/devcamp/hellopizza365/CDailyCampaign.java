package com.devcamp.hellopizza365;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@CrossOrigin
@RestController
public class CDailyCampaign {
    @CrossOrigin
    @GetMapping("/devcamp-date")
    public String getDateViet() {
        DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        return String.format("%s, mua 1 tặng 1.", dtfVietnam.format(today));
    }
}
