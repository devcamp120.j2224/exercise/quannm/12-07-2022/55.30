package com.devcamp.hellopizza365;

import java.util.Date;

public class CDrink {
    private String maNuocUong;
    private String tenNuocUong;
    private int donGia;
    private String ghiChu;
    private Date ngayTao;
    private Date ngayCapNhat;

    public CDrink(String maNuocUong, String tenNuocUong, int donGia) {
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ngayTao = new Date();
        this.ngayCapNhat = new Date();
    }
    public String getMaNuocUong() {
        return maNuocUong;
    }
    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }
    public String getTenNuocUong() {
        return tenNuocUong;
    }
    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }
    public int getDonGia() {
        return donGia;
    }
    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }
    public String getGhiChu() {
        return ghiChu;
    }
    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }
    public Date getNgayTao() {
        return ngayTao;
    }
    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }
    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }
    @Override
    public String toString() {
        return "{maNuocUong=" + this.maNuocUong
                + ",tenNuocUong=" + this.tenNuocUong
                + ",donGia=" + this.donGia
                + ",ghiChu=" + this.ghiChu
                + ",ngayTao=" + this.ngayTao
                + ",ngayCapNhat=" + this.ngayCapNhat + "}";
    }
}
